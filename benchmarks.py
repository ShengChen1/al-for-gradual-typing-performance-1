import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from dataset import data_loader as loader
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.svm import SVR, SVC
from sklearn.neural_network import MLPClassifier, MLPRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error as MSE
from sklearn.datasets import load_iris
from alipy.experiment.al_experiment import AlExperiment
from math import sqrt
import pickle


# Seed = 0
# np.random.seed(Seed)
CROSS_VALIDATION = 10



X, y = loader.raytrace()

Loss = 0
for cv in range(CROSS_VALIDATION):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.7)
    model = MLPRegressor(max_iter=300)
    # model = SVR()
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    loss = sqrt(MSE(y_test.ravel(), y_pred.ravel()))
    print(loss)
    Loss += loss

print(10*'+')
print(Loss / CROSS_VALIDATION)



