import torch
import torch.nn.functional as F
from torch.nn import Linear, ReLU
from torch_geometric.nn import GCNConv


class Net(torch.nn.Module):
    def __init__(self, NetArch):
        super(Net, self).__init__()
        self.conv1 = GCNConv(NetArch[0], 16)
        self.conv2 = GCNConv(16, NetArch[-1])

    def forward(self, data):
        x, edge_index = data.x, data.edge_index

        x = self.conv1(x, edge_index)
        x = F.relu(x)
        # x = F.dropout(x, training=self.training)
        x = self.conv2(x, edge_index)

        return F.log_softmax(x, dim=1)

class GCN(torch.nn.Module):
    def __init__(self, NetArch):
        super(GCN, self).__init__()
        self.conv1 = GCNConv(NetArch[0], NetArch[1])
        self.conv2 = GCNConv(NetArch[1], NetArch[2])
        self.fc1 = Linear(NetArch[2], NetArch[3])
        self.fc2 = Linear(NetArch[3], NetArch[-1])
        self.act1 = ReLU()
        self.act2 = ReLU()

    def forward(self, data):
            x, edge_index = data.x, data.edge_index
            x = self.conv1(x, edge_index)
            x = F.relu(x)
            x = self.conv2(x, edge_index)
            x = F.relu(x)
            logit = x
            x = self.fc1(x)
            x = self.act1(x)
            x = self.fc2(x)
            x = self.act2(x)
            return logit, F.log_softmax(x, dim=1)

class TACA(torch.nn.Module):
    def __init__(self, NetArch):
        super(TACA, self).__init__()
        self.conv1 = GCNConv(NetArch[0], NetArch[1])
        self.conv2 = GCNConv(NetArch[1], NetArch[-1])

    def forward(self, data):
        x, edge_index = data.x, data.edge_index

        x = self.conv1(x, edge_index)
        x = F.relu(x)
        logits = x
        # x = F.dropout(x, training=self.training)
        x = self.conv2(x, edge_index)

        return logits, F.log_softmax(x, dim=1)