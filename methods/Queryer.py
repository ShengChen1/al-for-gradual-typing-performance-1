import torch
import numpy as np
from utils.kcenter_greedy import lloyd
from utils.pairwise import pairwise_distance
torch.manual_seed(0)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def K_Center_Greedy(X, k):
    '''
    :param X: tensors of all data points
           k: number of clusters
    :return: the cloest points to the centroids
    '''
    choice_cluster, initial_state = lloyd(X, k, device=device)
    select_idx = []
    for i, centroid in enumerate(initial_state):
        distance = (X-centroid.view(1, X.size()[1])).norm(p=2, dim=1)
        # print(distance)
        value, idx = distance.min(0)
        # print(idx)
        # print(value)
        select_idx.append(idx.item())
    return select_idx

