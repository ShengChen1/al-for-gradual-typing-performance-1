from alipy.index import IndexCollection
import numpy as np
from sklearn.metrics import mean_squared_error as MSE
from sklearn.model_selection import train_test_split
from math import sqrt

n_per_quary = 3

def RMSE(y, y_):
    return np.sqrt(np.square(y - y_))

def get_random_sample_idx(remain_id_list):
    random_list = np.random.choice(range(len(remain_id_list)), n_per_quary, replace=False)
    query_idx = [remain_id_list[i] for i in random_list]
    return IndexCollection(query_idx)

class RandomSampling():

    def __init__(self, X, y, StartRatio=0.1, n_Query=50, TestSize = 0.1):
        self.X, self.X_test, self.y, self.y_test = train_test_split(X, y, test_size=TestSize)
        self.ratio = StartRatio
        self.N = n_Query
        self.loss = []
        self.sampleSize = []

    def train(self, learner):
        pool_idx = IndexCollection(np.arange(len(self.X)))
        start_idx = IndexCollection.random_sampling(pool_idx, rate=self.ratio)
        remain_idx = pool_idx.difference_update(start_idx)
        for iter in range(self.N):
            X_start, y_start = self.X[start_idx], self.y[start_idx]
            X_remain, y_remain = self.X[remain_idx], self.y[remain_idx]
            if iter == 0:
                learner.fit(X_start, y_start)
            else:
                learner.partial_fit(X_start, y_start)
            y_pred = learner.predict(self.X_test)
            loss = MSE(self.y_test, y_pred)
            self.loss.append(sqrt(loss))
            # Record curve
            self.sampleSize.append(len(y_start))
            # Get random sample id
            query_idx = get_random_sample_idx(remain_idx)
            start_idx.update(query_idx)
            remain_idx.difference_update(query_idx)













