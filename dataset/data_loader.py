import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import os
# PATH = os.getcwd()
PATH = "D:/PyProject/Code-Config-Analyst"

def raytrace(clf = False):
    X, y = pd.read_csv(str(PATH) + '/dataset/raytrace_fea.csv').as_matrix(), \
           pd.read_csv(str(PATH) + '/dataset/raytrace_time.csv').as_matrix().reshape(-1,1)
    if clf == True:
        p1, p2 = np.percentile(y, 33), np.percentile(y, 66)
        y[y<p1], y[(y>=p1) & (y<=p2)], y[y>p2]  = 0, 1, 2
    return X, y

def meteor(clf = False):
    X, y = pd.read_csv(str(PATH) + '/dataset/meteor_bits.txt',sep=" ", header=None).as_matrix(), \
           pd.read_csv(str(PATH) + '/dataset/meteor_log.txt',sep=" ", header=None).as_matrix()
    X_arr, y_arr = [], []
    for i, (xi, yi) in enumerate(zip(X, y)):
        if i % 2 == 0 and xi == yi:
            pass
        else:
            xi = np.char.split(X[i].astype(str), sep =',')
            xi = np.array(xi[0]).astype(np.int)
            X_arr.append(xi)
            y_arr.append(float(y[i][0]))
    X, y = np.array(X_arr), np.array(y_arr).reshape(-1, 1)
    if clf == True:
        p1, p2 = np.percentile(y, 33), np.percentile(y, 66)
        y[y<p1], y[(y>=p1) & (y<=p2)], y[y>p2]  = 0, 1, 2
    return X, y

def nbody(clf = False):
    X, y = pd.read_csv(str(PATH) + '/dataset/nbody_bits.txt', sep=" ", header=None).as_matrix(), \
           pd.read_csv(str(PATH) + '/dataset/nbody_log.txt', sep=" ", header=None).as_matrix()
    X_arr, y_arr = [], []
    for i, (xi, yi) in enumerate(zip(X, y)):
        if i % 2 == 0 and xi == yi:
            pass
        else:
            xi = np.char.split(X[i].astype(str), sep=',')
            xi = np.array(xi[0]).astype(np.int)
            X_arr.append(xi)
            y_arr.append(float(y[i][0]))
    X, y = np.array(X_arr), np.array(y_arr).reshape(-1, 1)
    if clf == True:
        p1, p2 = np.percentile(y, 33), np.percentile(y, 66)
        y[y < p1], y[(y >= p1) & (y <= p2)], y[y > p2] = 0, 1, 2
    return X, y




if __name__ == "__main__":
    X, y = meteor()
    print(X[0], y)