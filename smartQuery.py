import torch
from torch_geometric.data import Data
from torch_geometric.datasets import TUDataset
from torch_geometric.datasets import Planetoid
import torch.nn.functional as F
from methods.network import Net, GCN, TACA
from utils.util import get_init_idx, after_random_quired_idx, \
    after_kcenter_quired_idx, get_unlabeled_logits, get_init_idx_k_center_greedy
import numpy as np
from methods.Queryer import K_Center_Greedy
import pickle
torch.manual_seed(0)

N_Query = 100
K = 5
maxEpoch = 42
N_Init = 20


Sample_Size, Random_Acc, TACA_Acc = [], [], []
Result = {'Sample_Size': [],
          'Random_Acc': [],
          'TACA_Acc':[]}

# dataset = TUDataset(root='/tmp/ENZYMES', name='ENZYMES') # This is for graph-level classification
dataset = Planetoid(root='/tmp/Cora', name='Cora')
Net_Architecture = [dataset.num_node_features,
                    16,
                    dataset.num_classes]
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = TACA(Net_Architecture).to(device)
data = dataset[0].to(device)
num_nodes = len(data.train_mask)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01, weight_decay=5e-4)

data.train_mask = get_init_idx_k_center_greedy(data, data.test_mask, K, N_Init).to(device)

# data.train_mask = get_init_idx(data.test_mask).to(device)

model.train()
for query in range(N_Query):
    i_center = query % K
    for epoch in range(maxEpoch):
        optimizer.zero_grad()
        logit, out = model(data)
        loss = F.nll_loss(out[data.train_mask], data.y[data.train_mask])
        loss.backward()
        optimizer.step()
        if epoch == 1:
            # Here we query the data points in the k-center greedy way
            unlabel_logits = get_unlabeled_logits(logit, data.train_mask)
            # print(unlabel_logits)
            near_centroids_idx = K_Center_Greedy(unlabel_logits, K)
            # print('done')
            data.train_mask = after_kcenter_quired_idx(data.train_mask, near_centroids_idx, i_center).to(device)

    model.eval()
    with torch.no_grad():
        _, pred = model(data)
        _, pred = pred.max(dim=1)
        correct = float(pred[data.test_mask].eq(data.y[data.test_mask]).sum().item())
        acc = correct / data.test_mask.sum().item()
        print('Validation ACC: {:.4f}'.format(acc))
        Result['TACA_Acc'].append(acc)

    print('Epoch # {:.1f}, loss: {:.4f}'.format(epoch, loss.item()))
    labeled_points = data.train_mask.sum().item()
    print('Queried {:.1f} # of instances'.format(labeled_points))
    Result['Sample_Size'].append(labeled_points)


# model.eval()
# _, pred = model(data)
# _, pred = pred.max(dim=1)
# correct = float (pred[data.test_mask].eq(data.y[data.test_mask]).sum().item())
# acc = correct / data.test_mask.sum().item()
# print('Accuracy: {:.4f}'.format(acc))

with open('Cora.result', 'wb') as f:
    pickle.dump(Result, f)



